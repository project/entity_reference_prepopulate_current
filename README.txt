Description
===========
Allows the contents of an "Entity Reference" field to be pre-populated by
taking entity from current page.

Install
=======
1. Download and enable the module.
2. Visit admin/structure/types/manage/[ENTITY-TYPE]/fields/[FIELD-NAME]
3. Enable "Entity reference prepopulate" under the instance settings.
4. Enable "Current Page Entity" under "Provider" list.


